This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Instructions

In the project directory, you can run:

### `npm start`

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The project contains a micro Node.js server to make authenticate with the Twitter API and make the actual API calls. This will run on  [http://localhost:5000](http://localhost:5000).

Running `npm start` will run both the server and the client.

The `package.json` should contain everything needed. A simple `yarn install` should suffice.

### Known Issues

On every search or scroll I reset the sorting to Default. This is done on purpose since I can't control the sorting Twitter does, therefor the sorting is only on the current batch anyway.

Ideally the scrolling from Twitter's API would allow sorting but at least this way it's clearer to the user that it as an artificial sort only on the current page.

