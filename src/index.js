import React from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import { createStore, applyMiddleware } from 'redux'

import createSagaMiddleware from 'redux-saga'
import {Provider} from 'react-redux';

import {composeWithDevTools} from 'redux-devtools-extension/logOnlyInProduction';
import * as serviceWorker from './serviceWorker';
import './css/index.css';
import App from './containers/App';
import mainSaga from './containers/App/saga'
import reducer from './reducers'

// create the saga middleware
const sagaMiddleware = createSagaMiddleware();
// mount it on the Store
const store = createStore(
  reducer,
  composeWithDevTools(applyMiddleware(sagaMiddleware))
);

// then run the saga
sagaMiddleware.run(mainSaga);

// render the application

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>
  , document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
