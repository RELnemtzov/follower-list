import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import {
  Card,
  CardBody,
  CardImg,
  CardSubtitle,
  CardText,
  CardTitle
} from 'reactstrap';

const FollowerCard = styled(Card).attrs({
  className: 'm-2'
})`
max-height: 20rem;
max-width: 10rem;
`;

const FollowerImg = styled(CardImg).attrs({
  className: 'm-auto w-100'
})`
`;

const FollowerCards = (props) => {
  if (!props.followers) {
    return null;
  }
  return (
    <div className="d-flex flex-wrap justify-content-between">
      {
        props.followers.map(follower => (
          <FollowerCard key={follower.screen_name} >
            <FollowerImg top src={ follower.profile_image_url.replace("_normal", "") } alt={ follower.screen_name } />
            <CardBody>
              <CardTitle className="font-weight-bold">
                <a href={`https://www.twitter.com/${ follower.screen_name }`} target="_blank" rel="noopener noreferrer">@{ follower.screen_name }</a>
              </CardTitle>
              <CardSubtitle className="font-italic"><i>{ follower.name }</i></CardSubtitle>
              <CardText className="text-sm-center text-muted text-truncate">{ follower.description }</CardText>
            </CardBody>
          </FollowerCard>
        ))
      }
    </div>
  );
};

FollowerCards.propTypes = {
  followers: PropTypes.arrayOf(PropTypes.object)
};

export default FollowerCards;