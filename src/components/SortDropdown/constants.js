export const sorts = {
  BY_ACCOUNT_NAME_ASC: {label: 'By Account Name', sortBy: ['name', 'asc']},
  BY_ACCOUNT_NAME_DESC: {label: 'By Account Name', sortBy: ['name', 'desc']},
  BY_SCREEN_NAME_ASC: {label: 'By Screen Name', sortBy: ['screen_name', 'asc']},
  BY_SCREEN_NAME_DESC: {label: 'By Screen Name', sortBy: ['screen_name', 'desc']},
};