import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import {
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  UncontrolledButtonDropdown
} from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faSortAlphaDown,
  faSortAlphaUp
} from '@fortawesome/free-solid-svg-icons';
import { faTwitter } from '@fortawesome/free-brands-svg-icons';
import { sorts } from './constants';

const SortLabel = styled.span.attrs({
  className: 'ml-2'
})`
font-size: 0.8rem;
`;

function SortDropdown({ sortFollowers, selectedSort}) {
  const handleSort = (sortBy) => {
    sortFollowers(...sortBy);
  };

  let sortIcon = faTwitter;
  let sortLabel = 'Default Sort';
  if (selectedSort) {
    sortIcon = selectedSort.sortBy[1] === 'asc'? faSortAlphaUp : faSortAlphaDown;
    sortLabel = selectedSort.label;
  }

  return (<UncontrolledButtonDropdown>
    <DropdownToggle caret>
      <span>
        <FontAwesomeIcon icon={sortIcon} />
        <span className="ml-1">{sortLabel}</span>
      </span>
    </DropdownToggle>
    <DropdownMenu>
      <DropdownItem header>By Account Name</DropdownItem>
      <DropdownItem onClick={() => handleSort(sorts.BY_ACCOUNT_NAME_ASC.sortBy)}>
        <FontAwesomeIcon icon={faSortAlphaUp} />
        <SortLabel>Ascending</SortLabel>
      </DropdownItem>
      <DropdownItem onClick={() => handleSort(sorts.BY_ACCOUNT_NAME_DESC.sortBy)}>
        <FontAwesomeIcon icon={faSortAlphaDown} />
        <SortLabel>Descending</SortLabel>
      </DropdownItem>
      <DropdownItem divider />
      <DropdownItem header>By Screen Name</DropdownItem>
      <DropdownItem onClick={() => handleSort(sorts.BY_SCREEN_NAME_ASC.sortBy)}>
        <FontAwesomeIcon icon={faSortAlphaUp} />
        <SortLabel>Ascending</SortLabel>
      </DropdownItem>
      <DropdownItem onClick={() => handleSort(sorts.BY_SCREEN_NAME_DESC.sortBy)}>
        <FontAwesomeIcon icon={faSortAlphaDown} />
        <SortLabel>Descending</SortLabel>
      </DropdownItem>
    </DropdownMenu>
  </UncontrolledButtonDropdown>);
}

SortDropdown.propTypes = {
  sortFollowers: PropTypes.func.isRequired,
  selectedSort: PropTypes.shape({
    label: PropTypes.string.isRequired,
    sortBy: PropTypes.arrayOf(PropTypes.string.isRequired),
  }),
};

export default SortDropdown;