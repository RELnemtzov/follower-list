import React from 'react';
import styled from 'styled-components';
import logo from '../../img/Twitter_Social_Icon_Circle_White.svg';

const ColorBanner = styled.div.attrs({
  className: 'w-100 mb-3'
})`
  height: 3rem;
  background-color: #1DA1F1;
  background-image: url(${logo});
  background-repeat: no-repeat;
  background-position: center;
  background-size: 2rem;
`;

class Banner extends React.PureComponent {
  render() {
    return (<ColorBanner/>)
  }
}

export default Banner;