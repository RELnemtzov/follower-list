import {createSelector} from 'reselect';
import {initialState} from './reducer';

const selectAppDomain = state => state.app || initialState;
const selectPrevCursor = state => selectAppDomain(state).prevCursor;
const selectNextCursor = state => selectAppDomain(state).nextCursor;
const selectFollowers = state => selectAppDomain(state).followers;

const makeSelectApp = () => createSelector(selectAppDomain, substate => substate);

export default makeSelectApp;

export {
  selectAppDomain,
  selectPrevCursor,
  selectNextCursor,
  selectFollowers,
};