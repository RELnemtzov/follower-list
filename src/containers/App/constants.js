import actionCreator, {ASYNC} from 'redux-action-types-creator';

export default actionCreator('APP')({
  GET_FOLLOWERS: ASYNC,
  PREV_FOLLOWERS: ASYNC,
  NEXT_FOLLOWERS: ASYNC,
});