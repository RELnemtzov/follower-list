
import types from './constants';

export default {
  getFollowers: (username) => ({
    type: types.GET_FOLLOWERS.REQUEST,
    username,
  }),
  prevFollowers: (username, prevCursor) => ({
    type: types.PREV_FOLLOWERS.REQUEST,
    username,
    prevCursor,
  }),
  nextFollowers: (username, nextCursor) => ({
    type: types.NEXT_FOLLOWERS.REQUEST,
    username,
    nextCursor,
  })
};