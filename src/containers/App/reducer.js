import constants from './constants';

export const initialState = {};

function appReducer(state = initialState, action) {
  switch (action.type) {
    case constants.GET_FOLLOWERS.SUCCESS:
    case constants.PREV_FOLLOWERS.SUCCESS:
    case constants.NEXT_FOLLOWERS.SUCCESS:
      return {
        ...state,
        prevCursor: action.data.prevCursor,
        nextCursor: action.data.nextCursor,
        followers: action.data.users
      };
    default:
      return state;
  }
}

export default appReducer;