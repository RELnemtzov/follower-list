import _ from 'lodash';
import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { createStructuredSelector } from 'reselect';
import { bindActionCreators, compose } from 'redux';
import {
  Button,
  ButtonGroup,
  Form,
  FormGroup,
  Input,
  InputGroup,
  InputGroupAddon,
  Row
} from 'reactstrap';
import {
  faAngleLeft,
  faAngleRight,
  faSearch
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import actions from './actions';
import './index.css';
import makeSelectApp, {
  selectFollowers,
  selectNextCursor,
  selectPrevCursor
} from './selectors';
import FollowerCards from '../../components/FollowerCards';
import Banner from '../../components/Banner';
import SortDropdown from '../../components/SortDropdown';
import { sorts } from '../../components/SortDropdown/constants';

function App(props) {
  const [username, setUsername] = useState('');
  const [followers, setFollowers] = useState([]);
  const [selectedSort, setSelectedSort] = useState(undefined);

  useEffect(() => {
    setFollowers(props.followers);
  }, [props.followers]);

  const handleChange = e => setUsername(e.target.value);
  const handleSearch = e => {
    e.preventDefault();
    setSelectedSort(undefined);
    props.actions.getFollowers(username);
  };
  const handleKeyPress = e => {
    if (e.key === 'Enter') {
      handleSearch(e)
    }
  };

  const handleGetPrev = () => {
    props.actions.prevFollowers(username, props.prevCursor);
    setSelectedSort(undefined);

  };

  const handleGetNext = () => {
    props.actions.nextFollowers(username, props.nextCursor);
    setSelectedSort(undefined);
  };

  const sortFollowers = (accessor, order) => {
    setSelectedSort(_.find(sorts, {sortBy: [accessor, order]}));
    setFollowers(_.orderBy(followers, [f=> f[accessor] && f[accessor].toLowerCase()], [order]))
  };

  return (
    <div className="App">
      <Banner/>
      <h1 className="text-dark">Follower List</h1>
      <h5>Ariel Nemtzov</h5>
      <div className="content-container">
        <FormGroup>
          <Form>
            <InputGroup>
              <InputGroupAddon addonType="prepend">@</InputGroupAddon>
              <Input placeholder="Please Enter a Screen Name" onChange={ handleChange } onKeyPress={handleKeyPress} />
              <InputGroupAddon addonType="append">
                <Button color="primary" onClick={handleSearch}>
                  <FontAwesomeIcon icon={faSearch} />
                </Button>
              </InputGroupAddon>
            </InputGroup>
          </Form>
        </FormGroup>
        <Row className="d-flex justify-content-between mx-3">
          <ButtonGroup>
            {props.prevCursor && <Button color="secondary" onClick={handleGetPrev}><FontAwesomeIcon icon={faAngleLeft} /></Button>}
            {props.nextCursor && <Button color="secondary" onClick={handleGetNext}><FontAwesomeIcon icon={faAngleRight} /></Button>}
          </ButtonGroup>

          {followers && <SortDropdown sortFollowers={sortFollowers} selectedSort={selectedSort} />}
        </Row>
        <FollowerCards followers={followers} />
      </div>
    </div>
  );
}

App.propTypes = {
  actions: PropTypes.objectOf(PropTypes.func),
  prevCursor: PropTypes.string,
  nextCursor: PropTypes.string,
  followers: PropTypes.arrayOf(PropTypes.object)
};

const mapStateToProps = createStructuredSelector({
  app: makeSelectApp(),
  prevCursor: selectPrevCursor,
  nextCursor: selectNextCursor,
  followers: selectFollowers
});

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch)
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);

export default compose(
  withConnect
)(App);
