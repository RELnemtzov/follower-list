import { all, call, put, takeLatest } from 'redux-saga/effects';
import { api } from '../../api';
import constants from './constants';

function* appMainWatcher() {
  yield takeLatest(constants.GET_FOLLOWERS.REQUEST, function* (action) {
    try {
      const {data} = yield call(api.get, `/followers/list/${action.username}`);
      yield put({
        type: constants.GET_FOLLOWERS.SUCCESS,
        data
      });
    } catch (e) {
      yield put({
        type: constants.GET_FOLLOWERS.FAILURE,
        error: e,
      });
      // eslint-disable-next-line no-console
      console.error(e)
    }
  });

  yield takeLatest(constants.PREV_FOLLOWERS.REQUEST, function* (action) {
    try {
      const {data} = yield call(api.get, `/followers/list/${action.username}?prevCursor=${action.prevCursor}`);
      yield put({
        type: constants.PREV_FOLLOWERS.SUCCESS,
        data
      });
    } catch (e) {
      yield put({
        type: constants.PREV_FOLLOWERS.FAILURE,
        error: e,
      });
      // eslint-disable-next-line no-console
      console.error(e)
    }
  });

  yield takeLatest(constants.NEXT_FOLLOWERS.REQUEST, function* (action) {
    try {
      const {data} = yield call(api.get, `/followers/list/${action.username}?nextCursor=${action.nextCursor}`);
      yield put({
        type: constants.NEXT_FOLLOWERS.SUCCESS,
        data
      });
    } catch (e) {
      yield put({
        type: constants.NEXT_FOLLOWERS.FAILURE,
        error: e,
      });
      // eslint-disable-next-line no-console
      console.error(e)
    }
  });

}

export default function* defaultSaga() {
  yield all([
    appMainWatcher(),
  ]);
}