const Twitter = require('twitter');
const axios = require('axios');
const apiConstants = require('../apiConstants');

const rfc1738Encode = str => encodeURIComponent(str)
  .replace(/!/g, '%21')
  .replace(/'/g, '%27')
  .replace(/\(/g, '%28')
  .replace(/\)/g, '%29')
  .replace(/\*/g, '%2A');

/**
 * This has the obvious pitfall of needing to be regenerated on Token fail. But I have decided to let that slide for simplicity...
 *
 * https://developer.twitter.com/en/docs/basics/authentication/overview/application-only
 */
const genBearerToken = (consumerKey, consumerSecret) => {
  // step 1
  const consumerTokenEncoded = rfc1738Encode(consumerKey);
  const consumerSecretEncoded = rfc1738Encode(consumerSecret);

  // step 2
  const consumerString = `${consumerTokenEncoded}:${consumerSecretEncoded}`;

  // step 3
  const basicBearerToken = Buffer.from(consumerString).toString('base64');
  const twitterOauth = axios.create({ baseURL: 'https://api.twitter.com/oauth2/token' });
  return twitterOauth.post('', 'grant_type=client_credentials', {headers: {Authorization: `Basic ${basicBearerToken}`, 'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'}});
};

const getTwitterClient = (consumerKey, consumerSecret) => genBearerToken(consumerKey, consumerSecret).then(r => {
  const accessToken = r.data.access_token;
  return new Twitter(apiConstants.TWITTER_API_AUTH(accessToken));
});

module.exports = {
  rfc1738Encode,
  genBearerToken,
  getTwitterClient,
};