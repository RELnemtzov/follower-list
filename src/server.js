const express = require('express');
const cors = require('cors');
const apiConstants = require('./apiConstants');
const twitterApiUtils = require('./utils/twitterApiUtils');


twitterApiUtils.getTwitterClient(apiConstants.consumerKey, apiConstants.consumerSecret)
  .then(twitterClient => {
    const app = express();
    app.use(cors());
    const port = 5000;

    app.get('/followers/list/:username', (req, res) => {
      if (req.query.prevCursor && req.query.nextCursor) {
        throw new Error("prevCursor and nextCursor are mutually exclusive");
      }

      const params = {
        screen_name: req.params.username,
        cursor: req.query.nextCursor || req.query.prevCursor,
        count: apiConstants.pageSize,
        skip_status: true,
      };
      twitterClient.get('followers/list', params, (error, followers) => {
        if (error) {
          // eslint-disable-next-line no-console
          console.error(error);
          throw new Error(error);
        }
        const result = {
          users: followers.users
        };
        if (followers.previous_cursor !== 0) {
          result.prevCursor = followers.previous_cursor_str;
        }
        if (followers.next_cursor !== 0) {
          result.nextCursor = followers.next_cursor_str;
        }
        res.json(result);
      });
    });

    app.listen(port, (err) => {
      if (err) {
        console.error('something bad happened', err)
      }

      console.log(`server is listening on ${port}`)
    })
  });
